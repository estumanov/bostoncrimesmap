package com.example

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object BostonCrimes extends App {
  val spark = SparkSession
    .builder()
    .getOrCreate()

  Logger.getLogger("org").setLevel(Level.WARN)

  import spark.implicits._

  val path1 = if (args.length < 3) {
    "crime.csv"
  } else {
    args(0)
  }

  val path2 = if (args.length < 3) {
    "offense_codes.csv"
  } else {
    args(1)
  }

  val path3 = if (args.length < 3) {
    "./output/"
  } else {
    args(2)
  }
  val crimesDF = spark.read.format("csv")
    .option("header", "true")
    .option("inferSchema", "true")
    .load(path1)
  crimesDF.createOrReplaceTempView("crimes")

  val codesDF = spark.read.format("csv")
    .option("header", "true")
    .option("inferSchema", "true")
    .load(path2)
  codesDF.createOrReplaceTempView("codes")

  spark.sql("select DISTRICT,YEAR,MONTH,count(*) as count from crimes group by DISTRICT,YEAR,MONTH order by count")
    .createOrReplaceTempView("countByMonth")

  spark.sql("SELECT DISTRICT as DISTRICT,percentile_approx(count, 0.5) as crimes_monthly FROM countByMonth group by DISTRICT order by DISTRICT")
    .createOrReplaceTempView("median")

  crimesDF
    .groupBy($"DISTRICT")
    .agg(count("INCIDENT_NUMBER").name("crimes_total"),
      avg("Lat") as "lat",
      avg("Long") as "lng")
    .createOrReplaceTempView("geo")

  crimesDF.join(broadcast(codesDF.dropDuplicates("CODE")), $"OFFENSE_CODE" === $"CODE", "left_outer")
    .groupBy($"DISTRICT", $"NAME")
    .count()
    .sort('DISTRICT, 'count.desc)
    .createOrReplaceTempView("crime_type")

  spark.sql("select * from (SELECT DISTRICT, NAME, count,  row_number() OVER (partition by DISTRICT order by count desc) as country_rank " +
    "FROM crime_type) ranks where country_rank <= 3")
    .groupBy($"DISTRICT").agg(array_join(collect_list(regexp_replace($"NAME", ".-.*$", "") as "NAME"), ", ") as "frequent_crime_types")
    .createOrReplaceTempView("frequent_crime_type")

  val resultDF = spark.sql("SELECT a.DISTRICT, " +
    "a.crimes_total, " +
    "b.crimes_monthly, " +
    "c.frequent_crime_types, " +
    "a.lat, " +
    "a.lng FROM geo a " +
    "LEFT JOIN median b ON a.DISTRICT = b.DISTRICT " +
    "LEFT JOIN frequent_crime_type c ON a.DISTRICT = c.DISTRICT")

  resultDF
    .repartition(1)
    .write
    .mode("overwrite")
    .parquet(path3)

}